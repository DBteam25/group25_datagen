from setuptools import setup, find_packages

# run this script to initialize all packages lacking on your machine
# do in terminal: pip install -e .
setup(name="CaseStudy25_datagen_module", packages=find_packages())
