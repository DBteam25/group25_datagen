import generate_deals_app

# this is the entry point for running the app
if __name__ == "__main__":
    generate_deals_app.bootapp()
