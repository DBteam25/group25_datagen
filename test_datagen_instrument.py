import numpy, random
import time
from datetime import datetime, timedelta
import json
import pytest


########################################################################################################################
#                                    Tests for Instrument class constructor                                            #
########################################################################################################################

def test_instru_construct_no_args():
    with pytest.raises(Exception):
        i = Instrument()
        assert isinstance(i , Instrument)

def test_instru_construct_null_args():
    with pytest.raises(Exception):
        i = Instrument(none)
        assert isinstance(i , Instrument)

def test_instru_construct_blank_args():
    with pytest.raises(Exception):
        i = Instrument("")
        assert isinstance(i , Instrument)

def test_instru_construct_one_args():
    with pytest.raises(Exception):
        i = Instrument("something")
        assert isinstance(i , Instrument)

def test_instru_construct_two_args():
    with pytest.raises(Exception):
        i = Instrument("something", 300)
        assert isinstance(i , Instrument)

def test_instru_construct_three_args():
    with pytest.raises(Exception):
        i = Instrument("something", 300, 20)
        assert isinstance(i , Instrument)

def test_too_many_args():
    with pytest.raises(Exception):
        i = Instrument("something", 300, 20, 'a', 3.14)
        assert isinstance(i , Instrument)

def test_instru_construct_sufficient_args_but_wrong_types():
    with pytest.raises(Exception):
        i = Instrument(300, "wrong", '$', "str")
        assert isinstance(i , Instrument)
# successfully constructs an Instrument without raising an exception even though arguments are of the wrong types

def test_instru_construct_sufficient_args_with_blanks():
    with pytest.raises(Exception):
        i = Instrument(300, "", '$', "")
        assert isinstance(i , Instrument)
# successfully constructs an Instrument without raising an exception even though there are blank arguments


def test_instru_construct_sufficient_args_but_wrong_types_objects():
    with pytest.raises(Exception):
        x = Instrument("this one is OK", 100, 5, 2)
        i = Instrument(x, "this one is very very wrong", x, "str")
        assert isinstance(i , Instrument)
# successfully constructs an Instrument without raising an exception even though arguments are of the wrong types
# this one is even more extreme because I passed in another Instrument object


def test_instru_construct_sufficient_args_with_nulls():
    with pytest.raises(Exception):
        i = Instrument(300, none, '$', none)
        assert isinstance(i , Instrument)


def test_instru_construct_valid_args():
    i = Instrument("correct", 300, 12, 6)
    assert isinstance(i , Instrument)

def test_instru_construct_valid_args_massive_price():
    i = Instrument("correct", 9999999999999999999999999999999999999999999999999999999999999999999999999999999999, 12, 6)
    assert isinstance(i , Instrument)

def test_instru_construct_valid_args_tiny_price():
    i = Instrument("correct", 0.00000000000000000000000000000000000000000000000000000000000000000000000000000001, 12, 6)
    assert isinstance(i , Instrument)

def test_instru_construct_valid_args_massive_drift():
    i = Instrument("correct", 12, 9999999999999999999999999999999999999999999999999999999999999999999999999999999999, 6)
    assert isinstance(i , Instrument)

def test_instru_construct_valid_args_tiny_drift():
    i = Instrument("correct", 12, 0.00000000000000000000000000000000000000000000000000000000000000000000000000000001, 6)
    assert isinstance(i , Instrument)

def test_instru_construct_valid_args_massive_var():
    i = Instrument("correct", 12, 6, 9999999999999999999999999999999999999999999999999999999999999999999999999999999999)
    assert isinstance(i , Instrument)

def test_instru_construct_valid_args_tiny_var():
    i = Instrument("correct", 12, 6, 0.00000000000000000000000000000000000000000000000000000000000000000000000000000001)
    assert isinstance(i , Instrument)

def test_instru_construct_valid_args_long_name():
    i = Instrument("correcttttttttttttttttttttttttttttttttttttttttt2356586@#$%$&&*%(*fvbgnmghbvicjiojciooij", 100, 2, 4)
    assert isinstance(i , Instrument)


########################################################################################################################
#                                    Tests for Instrument.calculateNextPrice.(type)                                    #
########################################################################################################################
def test_calcprice_valid_argument_B():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert isinstance(instrument.calculateNextPrice('B'), float)

def test_calcprice_valid_argument_S():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert isinstance(instrument.calculateNextPrice('S'), float)


def test_calcprice_multiple_arguments():
    instrument = Instrument("guineapig", 100, 5, 10)
    with pytest.raises(Exception):
        assert (instrument.calculateNextPrice('B','S'))

def test_calcprice_blank_arguments():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert isinstance((instrument.calculateNextPrice('')), float)


def test_calcprice_null_argument():
    instrument = Instrument("guineapig", 100, 5, 10)
    with pytest.raises(Exception):
        assert (instrument.calculateNextPrice(none))

def test_calcprice_no_argument():
    instrument = Instrument("guineapig", 100, 5, 10)
    with pytest.raises(Exception):
        assert (instrument.calculateNextPrice())

def test_calcprice_invalid_argument_type_int():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert isinstance((instrument.calculateNextPrice(12)), float)
    # should default to 'S' without raising exception

def test_calcprice_invalid_argument_type_str():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert isinstance((instrument.calculateNextPrice("deutschedeutschedeutsche")), float)
    # should default to 'S' without raising exception


def test_calcprice_invalid_argument_type_wrong_char():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert isinstance(instrument.calculateNextPrice('T'), float)
    # should default to 'S' without raising exception


def test_calcprice_invalid_argument_type_special_char():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert isinstance(instrument.calculateNextPrice('#'), float)
    # should default to 'S' without raising exception

def test_calcprice_null_argument():
    instrument = Instrument("guineapig", 100, 5, 10)
    x = Instrument("wrench", 420, 20, 30)
    assert isinstance(instrument.calculateNextPrice(x), float)
    # should default to 'S' without raising exception

def test_calcprice_valid_result_price_S():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert (instrument.calculateNextPrice('S')) >= 0.0
    assert isinstance(instrument.calculateNextPrice('S'), float)

def test_calcprice_valid_price_B():
    instrument = Instrument("guineapig", 100, 5, 10)
    assert (instrument.calculateNextPrice('B')) >= 0.0
    assert isinstance(instrument.calculateNextPrice('B'), float)

########################################################################################################################

class Instrument:

    def __init__(self,
                 name,
                 base_price,
                 drift,
                 variance
                 ):
        self.name = name
        self.__price = base_price
        self.drift = drift
        self.variance = variance

    def calculateNextPrice(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0, 1) * self.variance + self.drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < newPrice * 0.4:
            self.drift = (-0.7 * self.drift)
        self.__price = newPrice * 1.01 if direction == 'B' else newPrice * 0.99
        return self.__price

    instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
                   "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
    counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")
    NUMBER_OF_RANDOM_DEALS = 2000
    TIME_PERIOD_MILLIS = 3600000
    EPOCH = datetime.now() - timedelta(days=1)

    class RandomDealData:
        def createInstrumentList(self):
            f = open('test_initialRandomValues.txt', 'r')
            instrumentId = 1000
            instrumentList = []
            for instrumentName in instruments:
                # get random value out of the txt file
                hashedValue = int(f.readline())
                isNegative = hashedValue < 0
                basePrice = (abs(hashedValue) % 10000) + 90.0
                drift = ((abs(hashedValue) % 5) * basePrice) / 1000.0
                drift = 0 - drift if isNegative else drift
                variance = (abs(hashedValue) % 1000) / 100.0
                variance = 0 - variance if isNegative else variance
                instrument = Instrument(instrumentName, basePrice, drift, variance)
                instrumentList.append(instrument)
                instrumentId += 1
            return instrumentList

        def createRandomData(self, instrumentList):
            time.sleep(random.uniform(1, 30) / 100)
            dealId = 20000
            instrument = instrumentList[numpy.random.randint(0, len(instrumentList))]
            cpty = counterparties[numpy.random.randint(0, len(counterparties))]
            type = 'B' if numpy.random.choice([True, False]) else 'S'
            quantity = int(numpy.power(1001, numpy.random.random()))
            dealTime = datetime.now() - timedelta(days=1)
            dealId += 1
            deal = {
                'instrumentName': instrument.name,
                'cpty': cpty,
                '__price': instrument.calculateNextPrice(type),
                'type': type,
                'quantity': quantity,
                'time': dealTime.strftime("%d-%b-%Y (%H:%M:%S.%f)"),
            }
            return json.dumps(deal)

