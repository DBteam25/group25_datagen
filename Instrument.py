import numpy


class Instrument:

    def __init__(self,
                 name,
                 base_price,
                 drift,
                 variance
                 ):
        if isinstance(name, str): self.name = name
        if isinstance(base_price, float): self.__price = base_price
        if isinstance(drift, float): self.drift = drift
        if isinstance(variance, float): self.variance = variance

    def calculateNextPrice(self, direction):
        newPriceStarter = self.__price + numpy.random.normal(0, 1) * self.variance + self.drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.__price < newPrice * 0.4:
            self.drift = (-0.7 * self.drift)
        self.__price = newPrice * 1.01 if direction == 'B' else newPrice * 0.99
        return self.__price
